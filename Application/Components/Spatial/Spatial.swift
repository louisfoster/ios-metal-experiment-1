//
//  Spatial.swift
//  frameBuf
//
//  Created by Louis Foster on 2/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation
import simd

protocol SpatialProtocol
{
    var positionX: Float { get set }
    var positionY: Float { get set }
    var positionZ: Float { get set }
    
    var rotationX: Float { get set }
    var rotationY: Float { get set }
    var rotationZ: Float { get set }
    
    var scaleX: Float { get set }
    var scaleY: Float { get set }
    var scaleZ: Float { get set }
}

extension SpatialProtocol
{
    func isSpatial( ) -> Bool
    {
        return true
    }
    
    func modelMatrix( ) -> float4x4
    {
        var matrix = float4x4( )
        matrix.translate( x: self.positionX, y: self.positionY, z: self.positionZ )
        matrix.rotateAroundX( x: self.rotationX, y: self.rotationY, z: self.rotationZ )
        matrix.scale( x: self.scaleX, y: self.scaleY, z: self.scaleZ )
        return matrix
    }
}
