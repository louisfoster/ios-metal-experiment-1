//
//  Node.swift
//  frameBuf
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 This is the base class to give objects life
 
 */

import Metal
import QuartzCore

protocol NodeProtocol
{
    var device: MTLDevice { get }
    var name: String { get }
    var time: CFTimeInterval { get set }
    
    // Function should update time
    func updateWithDelta( delta: CFTimeInterval )
}

extension NodeProtocol
{
    func isNode( ) -> Bool
    {
        return true
    }
}
