//
//  Physical.swift
//  frameBuf
//
//  Created by Louis Foster on 2/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation
import MetalKit
import simd

protocol PhysicalProtocol
{
    var vertexCount: Int { get set }
    var vertexBuffer: MTLBuffer { get set }
    
    func updateOnResize( with size: CGSize ) throws
    
    func render( encoder: MTLRenderCommandEncoder )
}

extension PhysicalProtocol
{
    func isPhysical( ) -> Bool
    {
        return true
    }
}
