//
//  UniformBufferProvider.swift
//
//  Created by Louis Foster on 25/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 TODO: Create decent docs for this system
 
 A delegate should implement an enum that conforms to UniformIterable,
 like so:
 
 enum ObjectUniform: Int, UniformIterable
 {
     case mirror
 
     var index: Int
     {
        return self.rawValue
     }
 
     var type: UniformType
     {
         switch self
         {
            case .mirror: return .Bool
         }
     }
 }
 
 Which is assigned to the uniforms array property, like so:
 
 var uniforms: [ UniformIterable ] = [
    .mirror,
 ] as [ ObjectUniform ]
 
 And can be used to update values, like so:
 
 bp.update( value: true, at: ObjectUniform.mirror.index )
 */

import Metal
import simd

enum UniformType
{
    case float4x4
    case Float
    case Bool
}

protocol UniformIterable
{
    var index: Int { get }
    var type: UniformType { get }
}

struct UniformDataItem
{
    var type: UniformType
    var value: Any? = nil
    var size: Int? = nil
    
    init( type _type: UniformType,
          value _value: Any? = nil,
          size _size: Int? = nil )
    {
        self.type = _type
        
        switch self.type
        {
        case .float4x4:
            self.value = _value ?? float4x4( )
            self.size = _size ?? MemoryLayout<Float>.size * float4x4.numberOfElements( )
        case .Float:
            self.value = _value ?? Float( 0.0 )
            self.size = _size ?? MemoryLayout<Float>.size
        case .Bool:
            self.value = _value ?? false
            self.size = _size ?? MemoryLayout<Bool>.size
        }
    }
}

protocol UniformsBufferProviderDelegate
{
    var device: MTLDevice { get }
    var uniforms: [ UniformIterable ] { get }
    var bufferProvider: UniformsBufferProvider { get set }
}

protocol UniformsBufferProviderProtocol
{
    var availableResourcesSemaphore: DispatchSemaphore { get }
    var inflightBuffersCount: Int { get }
    var delegate: UniformsBufferProviderDelegate { get }
    var uniformsBuffers: [MTLBuffer] { get }
    var availableBufferIndex: Int { get }
    var uniforms: [ UniformDataItem ] { get }
}

class UniformsBufferProvider: UniformsBufferProviderProtocol
{
    // MARK: Properties
    
    private(set) var availableResourcesSemaphore: DispatchSemaphore
    private(set) var inflightBuffersCount: Int
    private(set) var delegate: UniformsBufferProviderDelegate
    private(set) var availableBufferIndex: Int = 0
    
    private(set) lazy var uniforms: [ UniformDataItem ] = self.delegate.uniforms.map
    {
        uniform -> UniformDataItem in
        return UniformDataItem( type: uniform.type )
    }
    
    private(set) lazy var uniformsBuffers: [ MTLBuffer ] =
    {
        let size = self.totalSize( )
        var buffers = [ MTLBuffer ]( )
        
        for _ in 0...self.inflightBuffersCount - 1
        {
            if let uniformsBuffer =
                self.delegate.device.makeBuffer( length: size, options: [] )
            {
                buffers.append( uniformsBuffer )
            }
        }
        
        return buffers
    }()
    
    // MARK: Initialisers
    
    init( _ _delegate: UniformsBufferProviderDelegate,
          inflightBuffersCount _inflightBuffersCount: Int = 3 )
    {
        self.inflightBuffersCount = _inflightBuffersCount
        
        self.availableResourcesSemaphore = DispatchSemaphore( value: self.inflightBuffersCount )
        
        self.delegate = _delegate
    }
    
    deinit
    {
        for _ in 0...self.inflightBuffersCount
        {
            self.availableResourcesSemaphore.signal( )
        }
    }
    
    // MARK: Methods
    
    func nextBuffer( ) -> MTLBuffer
    {
        let buffer = self.uniformsBuffers[ self.availableBufferIndex ]
        let bufferPointer = buffer.contents( )
        var stride = 0
        
        for uniform in self.uniforms
        {
            switch uniform.type
            {
            case .float4x4:
                
                if var value: float4x4 = uniform.value as? float4x4,
                    let size = uniform.size
                {
                    memcpy( bufferPointer.advanced( by: stride ),
                            &value,
                            size )
                    stride += size
                }
                
            case .Float:
                
                if var value: Float = uniform.value as? Float,
                    let size = uniform.size
                {
                    memcpy( bufferPointer.advanced( by: stride ),
                            &value,
                            size )
                    
                    stride += size
                }
                
            case .Bool:
                
                if var value: Bool = uniform.value as? Bool,
                    let size = uniform.size
                {
                    memcpy( bufferPointer.advanced( by: stride ),
                            &value,
                            size )
                    
                    stride += size
                }
            }
        }
        
        self.availableBufferIndex += 1
        if self.availableBufferIndex == self.inflightBuffersCount
        {
            self.availableBufferIndex = 0
        }
        
        return buffer
    }
    
    func totalSize( ) -> Int
    {
        var sum = 0
        
        for uniform in self.uniforms
        {
            sum += uniform.size ?? 0
        }
        
        return UniformsBufferProvider.getChunkCorrectedBufferSize( sum )
    }
    
    func update( value: Any, at index: Int )
    {
        self.uniforms[ index ].value = value
    }
}

extension UniformsBufferProvider
{
    class func getChunkCorrectedBufferSize( _ size: Int ) -> Int
    {
        return size + ( size % 16 )
    }
}
