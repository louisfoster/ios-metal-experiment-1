//
//  ShaderProgramProvider.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Metal

class ShaderProgramProvider
{
    static func createShaderProgram( device: MTLDevice,
                                     library _library: MTLLibrary?,
                                     descriptor: MTLRenderPipelineDescriptor,
                                     vertex: String,
                                     fragment: String?,
                                     colorPixelFormat: MTLPixelFormat,
                                     depthPixelFormat: MTLPixelFormat? = nil,
                                     stencilPixelFormat: MTLPixelFormat? = nil
                                    ) throws -> MTLRenderPipelineState
    {
        guard let library = _library ?? device.makeDefaultLibrary( )
            else
        {
            throw ShaderError.noMetalLibrary
        }
        
        descriptor.colorAttachments[ 0 ].pixelFormat = colorPixelFormat
        descriptor.depthAttachmentPixelFormat = depthPixelFormat ?? .invalid
        descriptor.stencilAttachmentPixelFormat = stencilPixelFormat ?? .invalid
        
        let vertexFunction = library.makeFunction( name: vertex )
        descriptor.vertexFunction = vertexFunction
        
        if let _frag = fragment
        {
            descriptor.fragmentFunction = library.makeFunction( name: _frag )
        }
        else
        {
            descriptor.fragmentFunction = nil
        }
        
        guard let program = try? device.makeRenderPipelineState( descriptor: descriptor )
            else
        {
            throw ShaderError.noPipelineState
        }
        
        return program
    }
}
