//
//  CameraViews.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

struct CameraViews
{
    var scene: CameraProtocol
    var panel: CameraProtocol
    
    func updateProjectionMatrices( _ dimensions: ViewDimensions2D )
    {
        self.scene.updateProjectionMatrix( dimensions )
        self.panel.updateProjectionMatrix( dimensions )
    }
}
