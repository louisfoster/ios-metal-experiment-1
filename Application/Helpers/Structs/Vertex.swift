//
//  Vertex.swift
//  frameBuf
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

protocol VertexProtocol
{
    func floatBuffer( ) -> [ Float ]
}

struct VertexPositionColor: VertexProtocol
{
    var x, y, z: Float // Position data
    var r, g, b, a: Float // Color data
    
    func floatBuffer( ) -> [ Float ]
    {
        return [ x, y, z, r, g, b, a ]
    }
}

struct VertexPositionColorTex: VertexProtocol
{
    var x, y, z: Float // Position data
    var r, g, b, a: Float // Color data
    var s, t: Float // Tex data
    
    func floatBuffer( ) -> [ Float ]
    {
        return [ x, y, z, r, g, b, a, s, t ]
    }
}

struct VertexPosition2DTex: VertexProtocol
{
    var x, y: Float // Position data
    var s, t: Float // Tex data
    
    func floatBuffer( ) -> [ Float ]
    {
        return [ x, y, s, t ]
    }
}
