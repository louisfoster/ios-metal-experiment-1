//
//  RendererErrors.swift
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

enum MetalViewError: Error
{
    case noView
    case noDevice
}

enum RendererInitError: Error
{
    case noPipelineState
    case noCommandQ
    case noObject
    case noDevice
    case noDefaultMetalLibrary
    case shaderInitFail
}

enum ModelInitError: Error
{
    case noSampler
    case textureLoad
    case noDevice
    case noDrawableSize
    case noVertexData
    case noUniformsData
    case noUniformsBuffer
    case noDepthStencilState
    case noDepthStencilAttachments
    case unknown
}

enum ShaderError: Error
{
    case noMetalLibrary
    case noPipelineState
}

enum SceneInitError: Error
{
    case noDevice
    case shaderInitFail
    case cameraInitFail
    case noFramebuffer
    case noColorspace
}
