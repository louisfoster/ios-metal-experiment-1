//
//  MTKView+Extensions.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

extension MTKView
{
    func dimensions2D( ) -> ViewDimensions2D
    {
        return ViewDimensions2D( width: Float( self.bounds.size.width ),
                                 height: Float( self.bounds.size.height ) )
    }
}
