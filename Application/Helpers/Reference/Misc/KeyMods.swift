//
//  KeyMods.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

// The following code is used to detect key modifiers

func flagsChanged(with event: NSEvent)
{
    switch event.modifierFlags.intersection(.deviceIndependentFlagsMask)
    {
    case [.shift]:
        print("shift key is pressed")
    case [.control]:
        print("control key is pressed")
    case [.option] :
        print("option key is pressed")
    case [.command]:
        print("Command key is pressed")
    case [.control, .shift]:
        print("control-shift keys are pressed")
    case [.option, .shift]:
        print("option-shift keys are pressed")
    case [.command, .shift]:
        print("command-shift keys are pressed")
    case [.control, .option]:
        print("control-option keys are pressed")
    case [.control, .command]:
        print("control-command keys are pressed")
    case [.option, .command]:
        print("option-command keys are pressed")
    case [.shift, .control, .option]:
        print("shift-control-option keys are pressed")
    case [.shift, .control, .command]:
        print("shift-control-command keys are pressed")
    case [.control, .option, .command]:
        print("control-option-command keys are pressed")
    case [.shift, .command, .option]:
        print("shift-command-option keys are pressed")
    case [.shift, .control, .option, .command]:
        print("shift-control-option-command keys are pressed")
    default:
        print("no modifier keys are pressed")
    }
}
