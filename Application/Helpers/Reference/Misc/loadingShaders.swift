//
//  loadingShaders.swift
//  frameBuf
//
//  Created by Louis Foster on 7/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//


// The following is for the Core Image CILineOverlay filter pass
/*
guard let img = CIImage( mtlTexture: self.framebuffer.firstPass )
else
{
    return
}

let newImg = img.applyingFilter( self.lineFilter.name,
                                 parameters: self.lineFilter.params )
let background = CIImage( color: CIColor.white ).cropped( to: newImg.extent )
let finalImage = newImg.composited( over: background )

self.ciContext.render( finalImage,
                       to: self.framebuffer.drawableTexture,
                       commandBuffer: commandBuffer,
                       bounds: finalImage.extent,
                       colorSpace: colorspace )
*/

/* For a blur -> sobel MPS shader pass
 
let inPlaceTexture = UnsafeMutablePointer<MTLTexture>.allocate(capacity: 1)
inPlaceTexture.initialize( to: self.framebuffer.firstPass )

self.blur.encode( commandBuffer: commandBuffer,
                  inPlaceTexture: inPlaceTexture )

self.sobel.encode( commandBuffer: commandBuffer,
                   sourceTexture: self.framebuffer.firstPass,
                   destinationTexture: self.framebuffer.drawableTexture )
*/
