//
//  CubeUniforms.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation
import simd

enum CubeUniforms
{
    case projectionMatrix(float4x4)
    case cubeModelMatrix(float4x4)
    case time(Float)
    case overrideColor(Float)
    case mirrorTexture(Bool)
}

struct CubeUniformsData
{
    var projectionMatrix = CubeUniforms.projectionMatrix( float4x4( ) )
    var cubeModelMatrix = CubeUniforms.cubeModelMatrix( float4x4( ) )
    var time = CubeUniforms.time( Float( ) )
    var overrideColor = CubeUniforms.overrideColor( Float( ) )
    var mirrorTexture = CubeUniforms.mirrorTexture( Bool( ) )
 
    static func data( for _uniform: CubeUniforms ) -> ( raw: Any, size: Int )
    {
        let raw = CubeUniformsData.raw(value: _uniform)
        let size = CubeUniformsData.size(of: _uniform)
        return ( raw: raw, size: size )
    }
 
    static func size( of _uniform: CubeUniforms ) -> Int
    {
        switch _uniform
        {
        case .projectionMatrix(_), .cubeModelMatrix(_):
            return MemoryLayout<Float>.size * float4x4.numberOfElements( )
        case .time(_), .overrideColor(_):
            return MemoryLayout<Float>.size
        case .mirrorTexture(_):
            return MemoryLayout<Bool>.size
        }
    }
 
    static func raw( value _uniform: CubeUniforms ) -> Any
    {
        switch _uniform {
        case let .projectionMatrix(matrix):
            return matrix
        case let .cubeModelMatrix(matrix):
            return matrix
        case let .time(time):
            return time
        case let .overrideColor(color):
            return color
        case let .mirrorTexture(mirror):
            return mirror
        }
    }
}
