//
//  UniformBuffer.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

protocol UniformBufferProtocol { }

class UniformBuffer: UniformBufferProtocol
{
    var uniformsBuffers: [ MTLBuffer ]
    var availableBufferIndex: Int
    var uniforms: [ ( uniform: Any, size: Int ) ]
    var inflightBuffersCount: Int
    var availableResourcesSemaphore: DispatchSemaphore
    var device: MTLDevice
    
    init( device _device: MTLDevice, buffers: Int = 3 )
    {
        self.uniformsBuffers = [ MTLBuffer ]()
        self.availableBufferIndex = 0
        self.inflightBuffersCount = buffers
        self.uniforms = [ ( uniform: Any, size: Int ) ]()
        self.availableResourcesSemaphore = DispatchSemaphore( value: self.inflightBuffersCount )
        self.device = _device
    }
    
    deinit
    {
        for _ in 0...self.inflightBuffersCount
        {
            self.availableResourcesSemaphore.signal( )
        }
    }
    
    private func getBufferSize( ) -> Int
    {
        var sum = 0
        
        for item in self.uniforms
        {
            sum += item.size
        }
        
        sum += sum
        
        return sum
    }
    
    private func updateBuffers( )
    {
        var buffers = [ MTLBuffer ]( )
        let bufferSize = self.getBufferSize( )
        
        for _ in 0...self.inflightBuffersCount - 1
        {
            if let uniformsBuffer = self.device.makeBuffer( length: bufferSize,
                                                            options: [ ] )
            {
                buffers.append( uniformsBuffer )
            }
        }
        
        self.uniformsBuffers = buffers
    }
    
    func add( uniform _uniform: Any, size: Int )
    {
        self.uniforms.append( ( uniform: _uniform, size: size ) )
        
        self.updateBuffers( )
    }
    
    func update( uniform _uniform: Any, at index: Int )
    {
        self.uniforms[ index ].uniform = _uniform
    }
    
    func nextUniformsBuffer( ) -> MTLBuffer
    {
        let buffer = self.uniformsBuffers[ self.availableBufferIndex ]
        let bufferPointer = buffer.contents( )
        
        var stride = 0
        
        for var item in self.uniforms
        {
            memcpy( bufferPointer.advanced(by: stride),
                    &item.uniform,
                    item.size )
            
            stride += item.size
        }
        
        self.availableBufferIndex += 1
        if self.availableBufferIndex == self.inflightBuffersCount
        {
            self.availableBufferIndex = 0
        }
        
        return buffer
    }
}
