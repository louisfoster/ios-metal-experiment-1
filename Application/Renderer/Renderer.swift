//
//  Renderer.swift
//  frameBuf
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 The renderer is called by the view controller to instantiate
 the visual world itself after the view itself has been
 created and the Metal instance has been established
 
 */

import MetalKit
import simd

protocol RendererProtocol: class, MTKViewDelegate
{
    var commandQ: MTLCommandQueue { get }
    var lastFrameTimestamp: CFTimeInterval { get }
    
    var scene: Scene { get }
}

extension RendererProtocol
{
    func render( _ view: MTKView )
    {
        self.scene.render( view, self.commandQ )
    }
    
    func updateLogic( timeSinceLastUpdate: CFTimeInterval )
    {
        self.scene.updateWithDelta( delta: timeSinceLastUpdate )
    }
    
    func updateRenderable( with size: CGSize )
    {
        do
        {
            try self.scene.updateOnResize( with: size )
        }
        catch
        {
            print( "Resize error" )
        }
    }
}

class Renderer: NSObject, RendererProtocol
{
    // MARK: Properties
    
    private(set) var commandQ: MTLCommandQueue
    
    private(set) var lastFrameTimestamp: CFTimeInterval
    
    private(set) var scene: Scene

    // MARK: Initialization
    
    init( _ _view: MTKView ) throws
    {
        guard let _device = _view.device
        else
        {
            throw RendererInitError.noDevice
        }
        
        guard let _commandQ = _device.makeCommandQueue( )
        else
        {
            throw RendererInitError.noCommandQ
        }
        
        _view.preferredFramesPerSecond = 30
        _view.clearColor = MTLClearColor( red: 1.0,
                                          green: 1.0,
                                          blue: 1.0,
                                          alpha: 1.0 )
        
        self.commandQ = _commandQ
        
        self.lastFrameTimestamp = 0.0
        
        let textureLoader = MTKTextureLoader( device: _device )
        
        self.scene = try Scene( textureLoader, _view )
    }
}

// MARK: - MTKViewDelegate Methods
extension Renderer
{
    func draw( in _view: MTKView )
    {
        self.render( _view )
        
        if self.lastFrameTimestamp == 0.0
        {
            self.lastFrameTimestamp = CACurrentMediaTime()
        }

        let elapsed = CACurrentMediaTime() - self.lastFrameTimestamp
        self.lastFrameTimestamp = CACurrentMediaTime()

        self.updateLogic( timeSinceLastUpdate: elapsed )
    }
    
    func mtkView( _ _view: MTKView, drawableSizeWillChange size: CGSize )
    {
        self.updateRenderable( with: size )
    }
}
