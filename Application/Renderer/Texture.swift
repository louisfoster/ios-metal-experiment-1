//
//  Texture.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

class Texture { }

extension Texture
{
    static func load( named name: String,
                      loader _textureLoader: MTKTextureLoader )
    throws -> MTLTexture
    {
        do
        {
            let texture: MTLTexture =
                try _textureLoader.newTexture( name: name,
                                               scaleFactor: 1.0,
                                               bundle: nil,
                                               options: nil )
            return texture
        }
        catch ModelInitError.textureLoad
        {
            print( "Texture \(name) not loaded" )
            throw ModelInitError.textureLoad
        }
    }
    
    static func defaultSampler( device _device: MTLDevice ) throws -> MTLSamplerState
    {
        let sampler = MTLSamplerDescriptor( )
        sampler.minFilter = .linear
        sampler.magFilter = .linear
        sampler.mipFilter = .linear
        sampler.maxAnisotropy = 1
        sampler.sAddressMode = .clampToEdge
        sampler.tAddressMode = .clampToEdge
        sampler.rAddressMode = .clampToEdge
        sampler.normalizedCoordinates = true
        sampler.lodMinClamp = 0
        sampler.lodMaxClamp = .greatestFiniteMagnitude
        
        guard let _sampler = _device.makeSamplerState( descriptor: sampler )
            else
        {
            throw ModelInitError.noSampler
        }
        
        return _sampler
    }
}
