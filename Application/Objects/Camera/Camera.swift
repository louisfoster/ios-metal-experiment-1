//
//  Camera.swift
//  frameBuf
//
//  Created by Louis Foster on 30/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit
import simd

protocol CameraProtocol: class, NodeProtocol, SpatialProtocol
{
    /*
     camera has position, and therefore an inverse world matrix
     when the drawable area size changes, it's proj matrix updates
     
     a node could have a camera, and a specific camera node can
     then access the node's rotation, position, model view matrix
     and all the camera needs is the ability to invert the mvm
     and be provided with updates to the camera with size change
 
    */
    var fieldOfView: Float { get set }
    var nearPlane: Float { get set }
    var farPlane: Float { get set }
    var projectionMatrix: float4x4 { get }
    
    // Invert model matrix from parent node
    func getViewMatrix( ) -> float4x4
    // update projection matrix with new dimensions
    func updateProjectionMatrix( _ _dimensions: ViewDimensions2D )
    
    func tilt( degrees: Float )
}

extension CameraProtocol
{
    func isCamera( ) -> Bool
    {
        return true
    }
}

class Camera: CameraProtocol
{
    // MARK: Properties
    
    var device: MTLDevice
    
    var name: String
    
    var time: CFTimeInterval
    
    var fieldOfView: Float
    
    var nearPlane: Float
    
    var farPlane: Float
    
    var positionX: Float
    
    var positionY: Float
    
    var positionZ: Float
    
    var rotationX: Float
    
    var rotationY: Float
    
    var rotationZ: Float
    
    var scaleX: Float
    
    var scaleY: Float
    
    var scaleZ: Float
    
    var dimensions: ViewDimensions2D
    
    private(set) lazy var projectionMatrix: float4x4 = self.buildProjectionMatrix( )
    
    // MARK: Initialisers
    
    init( name _name: String,
          device _device: MTLDevice,
          dimensions _dimensions: ViewDimensions2D,
          x: Float = 0,
          y: Float = 0,
          z: Float = 0 )
    {
        self.name = _name
        self.device = _device
        self.time = 0.0
        
        self.positionX = x
        self.positionY = y
        self.positionZ = z
        self.rotationX = 0.0
        self.rotationY = 0.0
        self.rotationZ = 0.0
        
        self.scaleX = 1.0
        self.scaleY = 1.0
        self.scaleZ = 1.0
        
        self.fieldOfView = 85.0
        self.nearPlane = 0.01
        self.farPlane = 100.0
        
        self.dimensions = _dimensions
    }
    
    // MARK: Camera Methods
    
    func getViewMatrix( ) -> float4x4
    {
        return self.modelMatrix( ).inverse
    }
    
    func updateProjectionMatrix( _ _dimensions: ViewDimensions2D )
    {
        self.projectionMatrix = self.buildProjectionMatrix( dimensions: _dimensions )
    }
    
    private func buildProjectionMatrix( dimensions _dimensions: ViewDimensions2D? = nil,
                                        fieldOfView _fov: Float? = nil,
                                        nearPlane _near: Float? = nil,
                                        farPlane _far: Float? = nil ) -> float4x4
    {
        self.dimensions = _dimensions ?? self.dimensions
        self.fieldOfView = _fov ?? self.fieldOfView
        self.nearPlane = _near ?? self.nearPlane
        self.farPlane = _far ?? self.farPlane
        
        let aspectRatio = self.dimensions.width / self.dimensions.height
        return float4x4.makePerspectiveViewAngle(
            float4x4.degrees( toRad: self.fieldOfView ),
            aspectRatio: aspectRatio,
            nearZ: self.nearPlane,
            farZ: self.farPlane )
    }
    
    func tilt( degrees: Float )
    {
        self.rotationX = float4x4.degrees( toRad: degrees )
    }
}

// MARK: - Node component methods
extension Camera
{
    func updateWithDelta(delta: CFTimeInterval)
    {
        self.time += delta
    }
}
