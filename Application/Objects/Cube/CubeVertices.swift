//
//  CubeBuilder.swift
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

extension Cube
{
    private static func planeVertices( ) -> [ VertexPositionColorTex ]
    {
        let A = VertexPositionColorTex( x: -2.0, y: -1.0, z: 2.0,
                                        r: 0.0, g: 0.0, b: 0.0, a: 1.0,
                                        s: 0.0, t: 1.0 )
        
        let B = VertexPositionColorTex( x: 2.0, y: -1.0, z: -2.0,
                                        r: 0.0, g: 0.0, b: 0.0, a: 1.0,
                                        s: 1.0, t: 0.0 )
        
        let C = VertexPositionColorTex( x: -2.0, y: -1.0, z: -2.0,
                                        r: 0.0, g: 0.0, b: 0.0, a: 1.0,
                                        s: 0.0, t: 0.0 )
        
        let D = VertexPositionColorTex( x: 2.0, y: -1.0, z: 2.0,
                                        r: 0.0, g: 0.0, b: 0.0, a: 1.0,
                                        s: 1.0, t: 1.0 )
        
        return [
            A, B, C,
            A, D, B,
        ]
    }
    
    internal static func vertices( ) -> [ VertexPositionColorTex ]
    {
        let d: Float = 1.0 // size ( distance from origin )
        
        let positions: [ Float ] = [ // x, y, z
            -d, -d, -d, // 0
             d, -d, -d, // 1
             d,  d, -d, // 2
            -d,  d, -d, // 3
            -d, -d,  d, // 4
             d, -d,  d, // 5
             d,  d,  d, // 6
            -d,  d,  d, // 7
        ]
        
        let faces: [ Int ] = [ // Quad: Triangle 1 verts, Triangle 2 verts
            4, 6, 7,  4, 5, 6, // Front
            5, 2, 6,  5, 1, 2, // Right
            1, 3, 2,  1, 0, 3, // Rear
            0, 7, 3,  0, 4, 7, // Left
            7, 2, 3,  7, 6, 2, // Top
            0, 5, 4,  0, 1, 5, // Base
        ]
        
        let color: [ Float ] = [ 1.0, 1.0, 1.0, 1.0 ] // r, g, b, a
        
        let texcoords: [ Float ] = [ // s, t
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
            1.0, 0.0,
        ]
        
        var _vertices = [ VertexPositionColorTex ]()
        
        for i in 0..<faces.count
        {
            let v: Int = faces[ i ] * 3 // get starting index of vert coords
            let t: Int = ( i % 6 ) * 2 // start index of tex
            
            let vertex = VertexPositionColorTex( x: positions[ v ],
                                                 y: positions[ v + 1 ],
                                                 z: positions[ v + 2 ],
                                                 r: color[ 0 ],
                                                 g: color[ 1 ],
                                                 b: color[ 2 ],
                                                 a: color[ 3 ],
                                                 s: texcoords[ t ],
                                                 t: texcoords[ t + 1 ] )
            
            _vertices.append( vertex )
        }
        
        // Create the reflection surface
        _vertices.append( contentsOf: Cube.planeVertices( ) )
        
        return _vertices
    }
}
