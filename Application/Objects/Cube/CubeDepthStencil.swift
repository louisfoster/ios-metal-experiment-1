//
//  CubeDepthStencil.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

struct CubeDepthStencilState
{
    var depthOnly, setStencil, testStencil: MTLDepthStencilState
}

struct CubeDepthStencilAttachment
{
    var depth: MTLRenderPassDepthAttachmentDescriptor
    var stencil: MTLRenderPassStencilAttachmentDescriptor
}

extension Cube
{
    class internal func getDepthStencilState( _ _device: MTLDevice )
        throws -> CubeDepthStencilState
    {
        let depthStencilDescriptor = MTLDepthStencilDescriptor( )
        let stencilDescriptor = MTLStencilDescriptor( )
        
        // First state, pre-stencil test
        depthStencilDescriptor.isDepthWriteEnabled = true
        depthStencilDescriptor.depthCompareFunction = .less // further away discard
        
        guard let stateDepthOnly =
            _device.makeDepthStencilState( descriptor: depthStencilDescriptor )
            else
        {
            throw ModelInitError.noDepthStencilState
        }
        
        // Second state, stencil setting without depth mask
        depthStencilDescriptor.isDepthWriteEnabled = false
        stencilDescriptor.stencilCompareFunction = .always
        stencilDescriptor.stencilFailureOperation = .keep
        stencilDescriptor.depthFailureOperation = .keep
        stencilDescriptor.depthStencilPassOperation = .replace
        depthStencilDescriptor.frontFaceStencil = stencilDescriptor
        depthStencilDescriptor.backFaceStencil = stencilDescriptor
        
        guard let stateSetStencilNoDepth =
            _device.makeDepthStencilState( descriptor: depthStencilDescriptor )
            else
        {
            throw ModelInitError.noDepthStencilState
        }
        
        // Final state, stencil testing with depth mask
        depthStencilDescriptor.isDepthWriteEnabled = true
        stencilDescriptor.stencilCompareFunction = .equal
        stencilDescriptor.stencilFailureOperation = .keep
        stencilDescriptor.depthFailureOperation = .keep
        stencilDescriptor.depthStencilPassOperation = .replace
        depthStencilDescriptor.frontFaceStencil = stencilDescriptor
        depthStencilDescriptor.backFaceStencil = stencilDescriptor
        
        guard let stateTestStencilWithDepth =
            _device.makeDepthStencilState( descriptor: depthStencilDescriptor )
            else
        {
            throw ModelInitError.noDepthStencilState
        }
        
        return CubeDepthStencilState( depthOnly: stateDepthOnly,
                                      setStencil: stateSetStencilNoDepth,
                                      testStencil: stateTestStencilWithDepth )
    }
    
    class internal func getDepthStencilAttachment( size: CGSize,
                                                   device: MTLDevice )
        throws -> CubeDepthStencilAttachment
    {
        // Create depth texture (depth used by entire scene)
        let depthTextureDescriptor =
            MTLTextureDescriptor.texture2DDescriptor( pixelFormat: .depth32Float_stencil8,
                                                      width: Int( size.width ),
                                                      height: Int( size.height ),
                                                      mipmapped: false )
        depthTextureDescriptor.storageMode = .private
        depthTextureDescriptor.usage = .renderTarget
        
        guard let depthStencilTexture =
            device.makeTexture( descriptor: depthTextureDescriptor )
            else
        {
            throw ModelInitError.noDepthStencilAttachments
        }
        
        depthStencilTexture.label = "DepthStencilTexture"
        
        // Create depth attachment with depth texture
        let depthAttachment = MTLRenderPassDepthAttachmentDescriptor( )
        depthAttachment.texture = depthStencilTexture
        depthAttachment.clearDepth = 1.0
        depthAttachment.storeAction = .dontCare
        depthAttachment.loadAction = .clear
        
        // Create stencil attachment with depth texture
        let stencilAttachment = MTLRenderPassStencilAttachmentDescriptor( )
        stencilAttachment.texture = depthStencilTexture
        stencilAttachment.storeAction = .dontCare
        stencilAttachment.clearStencil = 0
        stencilAttachment.loadAction = .clear
        
        return CubeDepthStencilAttachment( depth: depthAttachment,
                                           stencil: stencilAttachment )
    }
}
