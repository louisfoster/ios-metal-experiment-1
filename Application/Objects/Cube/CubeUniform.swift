//
//  CubeUniform.swift
//  frameBuf
//
//  Created by Louis Foster on 5/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation
import simd

enum CubeUniform: Int, UniformIterable
{
    case modelViewMatrix
    case projectionMatrix
    case time
    case overrideColor
    case mirrorTexture
    
    var index: Int
    {
        return self.rawValue
    }
    
    var type: UniformType
    {
        switch self
        {
        case .modelViewMatrix: return .float4x4
        case .projectionMatrix: return .float4x4
        case .time: return .Float
        case .overrideColor: return .Float
        case .mirrorTexture: return .Bool
        }
    }
}
