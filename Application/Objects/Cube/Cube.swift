//
//  Cube.swift
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit
import simd

protocol CubeProtocol: UniformsBufferProviderDelegate, SpatialProtocol { }

class Cube: CubeProtocol
{
    // MARK: Properties
    
    // Node
    
    var device: MTLDevice
    
    var name: String
    
    var time: CFTimeInterval
    
    // Spatial
    
    var positionX: Float
    
    var positionY: Float
    
    var positionZ: Float
    
    var rotationX: Float
    
    var rotationY: Float
    
    var rotationZ: Float
    
    var scaleX: Float
    
    var scaleY: Float
    
    var scaleZ: Float
    
    // Physical
    
    var vertexCount: Int
    
    var vertexBuffer: MTLBuffer
    
    // Physical Cube-specific
    
    var uniforms: [ UniformIterable ] = [
        .modelViewMatrix,
        .projectionMatrix,
        .time,
        .overrideColor,
        .mirrorTexture,
    ] as [ CubeUniform ]
    
    var textures: [ MTLTexture ]
    
    var samplerState: MTLSamplerState
    
    var projectionMatrix: float4x4
    
    var viewMatrix: float4x4
    
    var depthStencilState: CubeDepthStencilState
    
    var depthStencilAttachment: CubeDepthStencilAttachment
    
    lazy var bufferProvider: UniformsBufferProvider = UniformsBufferProvider( self )
    
    // MARK: Initialisers
    
    init( name: String,
          _ _mtkView: MTKView,
          _ _textures: [ MTLTexture ] ) throws
    {
        guard let _device = _mtkView.device
            else
        {
            throw ModelInitError.noDevice
        }
        
        // Node prop init
        self.name = name
        
        self.device = _device
        self.time = 0.0
        
        // Spatial prop init
        self.positionX = 0.0
        self.positionY = 0.0
        self.positionZ = 0.0
        self.rotationX = 0.0
        self.rotationY = 0.0
        self.rotationZ = 0.0
        
        self.scaleX = 1.0
        self.scaleY = 1.0
        self.scaleZ = 1.0
        
        // Physical prop init
        self.samplerState = try Texture.defaultSampler( device: self.device )
        
        guard let _drawableSize = _mtkView.currentDrawable?.layer.drawableSize
        else
        {
            throw ModelInitError.noDrawableSize
        }
        
        self.depthStencilAttachment =
            try Cube.getDepthStencilAttachment( size: _drawableSize, device: _device )
        
        self.depthStencilState = try Cube.getDepthStencilState( _device )
        
        self.textures = _textures
        
        let v = Cube.vertices( )
        
        var vertexData = Array<Float>( )
        
        for vertex in v
        {
            vertexData += vertex.floatBuffer( )
        }
        
        let dataSize = vertexData.count * MemoryLayout.size( ofValue: vertexData[ 0 ] )
        
        guard let _vertexBuffer = _device.makeBuffer( bytes: vertexData,
                                                      length: dataSize,
                                                      options: [ ] )
        else
        {
            throw ModelInitError.noVertexData
        }
        
        self.vertexBuffer = _vertexBuffer
        self.vertexCount = v.count
        
        self.projectionMatrix = float4x4( )
        self.viewMatrix = float4x4( )
    }
    
    private func draw( _ enc: MTLRenderCommandEncoder )
    {
        _ = self.bufferProvider.availableResourcesSemaphore
                               .wait( timeout: .distantFuture )
        
        enc.setCullMode( .front )
        enc.setStencilReferenceValue( 0xFF )
        
        // set depth stencil first state
        enc.setDepthStencilState( self.depthStencilState.depthOnly )
        
        // Handle shader textures
        enc.setFragmentTextures( self.textures, range: 0..<self.textures.count )
        enc.setFragmentSamplerState( self.samplerState, index: 0 )
        
        // Set shader vertex buffer
        enc.setVertexBuffer( self.vertexBuffer, offset: 0, index: 0 )
        
        // Handle shader uniform buffer
        var cubeModelMatrix = self.modelMatrix( )
        cubeModelMatrix.multiplyLeft( self.viewMatrix )
        
        self.bufferProvider.update( value: cubeModelMatrix,
                                    at: CubeUniform.modelViewMatrix.index )
        
        self.bufferProvider.update( value: self.projectionMatrix,
                                    at: CubeUniform.projectionMatrix.index )
        
        self.bufferProvider.update( value: Float( self.time ),
                                    at: CubeUniform.time.index )
        
        self.bufferProvider.update( value: Float( 1.0 ),
                                    at: CubeUniform.overrideColor.index )
        
        self.bufferProvider.update( value: false,
                                    at: CubeUniform.mirrorTexture.index )
        
        var uniformBuffer = self.bufferProvider.nextBuffer( )
        
        enc.setVertexBuffer( uniformBuffer, offset: 0, index: 1 )
        enc.setFragmentBuffer( uniformBuffer, offset: 0, index: 1 )
        
        // Draw Cube
        enc.drawPrimitives( type: .triangle,
                             vertexStart: 0,
                             vertexCount: 36,
                             instanceCount: 12 )
        
        // Set second state
        enc.setDepthStencilState( self.depthStencilState.setStencil )
        
        // Draw Reflection plane
        enc.drawPrimitives( type: .triangle,
                             vertexStart: 36,
                             vertexCount: 6,
                             instanceCount: 2 )
        
        // Final state
        enc.setDepthStencilState( self.depthStencilState.testStencil )
        
        // Draw reflected cube
        var reflectionModelMatrix = self.modelMatrix( )
        reflectionModelMatrix.translate( x: 0, y: -2.0, z: 0 )
        reflectionModelMatrix.multiplyLeft( self.viewMatrix )
        
        self.bufferProvider.update( value: reflectionModelMatrix,
                                    at: CubeUniform.modelViewMatrix.index )
        
        self.bufferProvider.update( value: Float( self.time ),
                                    at: CubeUniform.time.index )
        
        self.bufferProvider.update( value: Float( 0.3 ),
                                    at: CubeUniform.overrideColor.index )
        
        self.bufferProvider.update( value: true,
                                    at: CubeUniform.mirrorTexture.index )
        
        uniformBuffer = self.bufferProvider.nextBuffer( )
        
        enc.setVertexBuffer( uniformBuffer, offset: 0, index: 1 )
        enc.setFragmentBuffer( uniformBuffer, offset: 0, index: 1 )
        
        // Draw reflected cube
        enc.drawPrimitives( type: .triangle,
                             vertexStart: 0,
                             vertexCount: 36,
                             instanceCount: 12 )
    }
    
    func onDrawCompleted( )
    {
        self.bufferProvider.availableResourcesSemaphore.signal( )
    }
}

// MARK: - Node component methods
extension Cube: NodeProtocol
{
    func updateWithDelta(delta: CFTimeInterval)
    {
        self.time += delta
        
        self.rotationY = Float( self.time ) * float4x4.degrees( toRad: 180.0 )
    }
}

// MARK: - Physical component methods
extension Cube: PhysicalProtocol
{
    func updateOnResize( with size: CGSize ) throws
    {
        self.depthStencilAttachment =
            try Cube.getDepthStencilAttachment( size: size,
                                                device: self.device )
    }
    
    func render( encoder: MTLRenderCommandEncoder )
    {
        self.draw( encoder )
    }
}
