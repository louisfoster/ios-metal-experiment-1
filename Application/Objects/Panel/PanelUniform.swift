//
//  PanelUniform.swift
//  frameBuf
//
//  Created by Louis Foster on 8/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation
import simd

enum PanelUniform: Int, UniformIterable
{
    case modelViewMatrix
    case projectionMatrix
    
    var index: Int
    {
        return self.rawValue
    }
    
    var type: UniformType
    {
        switch self
        {
        case .modelViewMatrix: return .float4x4
        case .projectionMatrix: return .float4x4
        }
    }
}
