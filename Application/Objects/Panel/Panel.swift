//
//  Panel.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit
import simd

protocol PanelProtocol: UniformsBufferProviderDelegate, NodeProtocol, SpatialProtocol, PhysicalProtocol { }

class Panel: PanelProtocol
{
    // MARK: Properties
    
    // Node
    
    var device: MTLDevice
    
    var name: String
    
    var time: CFTimeInterval
    
    // Spatial
    
    var positionX: Float
    
    var positionY: Float
    
    var positionZ: Float
    
    var rotationX: Float
    
    var rotationY: Float
    
    var rotationZ: Float
    
    var scaleX: Float
    
    var scaleY: Float
    
    var scaleZ: Float
    
    // Physical
    
    var vertexCount: Int
    
    var vertexBuffer: MTLBuffer
    
    // Panel
    
    var samplerState: MTLSamplerState
    
    var textures: [ MTLTexture ]
    
    var uniforms: [ UniformIterable ] = [
        .modelViewMatrix,
        .projectionMatrix,
    ] as [ PanelUniform ]
    
    var projectionMatrix: float4x4
    
    var viewMatrix: float4x4
    
    lazy var bufferProvider: UniformsBufferProvider = UniformsBufferProvider( self )
    
    // MARK: Initialisers
    
    init( name: String,
          _ _mtkView: MTKView,
          _ _textures: [ MTLTexture ] ) throws
    {
        // Node prop init
        self.name = name
        
        guard let _device = _mtkView.device
            else
        {
            throw ModelInitError.noDevice
        }
        
        self.device = _device
        self.time = 0.0
        
        // Spatial prop init
        self.positionX = -2.8
        self.positionY = 1.5
        self.positionZ = -1.5
        self.rotationX = 0.0
        self.rotationY = 0.2
        self.rotationZ = 0.0
        
        let dimensions = _mtkView.dimensions2D( )

        self.scaleX = dimensions.width / dimensions.height
        self.scaleY = 1.0
        self.scaleZ = 1.0
        
        // Physical prop init
        
        let v = Panel.vertices( )
        
        var vertexData = Array<Float>( )
        
        for vertex in v
        {
            vertexData += vertex.floatBuffer( )
        }
        
        let dataSize = vertexData.count * MemoryLayout.size( ofValue: vertexData[ 0 ] )
        
        guard let _vertexBuffer = _device.makeBuffer( bytes: vertexData,
                                                      length: dataSize,
                                                      options: [ ] )
        else
        {
            throw ModelInitError.noVertexData
        }
        
        self.vertexBuffer = _vertexBuffer
        self.vertexCount = v.count
        
        // Panel prop init
        
        self.textures = _textures
        self.samplerState = try Texture.defaultSampler( device: self.device )
        
        self.projectionMatrix = float4x4( )
        self.viewMatrix = float4x4( )
    }
    
    // MARK: Draw calls
    
    func draw( _ enc: MTLRenderCommandEncoder, post: Bool )
    {
        if !post
        {
            _ = self.bufferProvider.availableResourcesSemaphore
                    .wait( timeout: .distantFuture )
        }
        
        enc.setCullMode( .front )
        
        enc.setFragmentSamplerState( self.samplerState, index: 0 )
        
        // Set shader vertex buffer
        enc.setVertexBuffer( self.vertexBuffer, offset: 0, index: 0 )
        
        // Handle shader textures
        if post
        {
            enc.setFragmentTextures( self.textures, range: 0..<self.textures.count )
        }
        else
        {
            enc.setFragmentTexture( self.textures[1], index: 0 )
            
            // Handle shader uniform buffer
            var panelModelMatrix = self.modelMatrix( )
            panelModelMatrix.multiplyLeft( self.viewMatrix )
            
            self.bufferProvider.update( value: panelModelMatrix,
                                        at: PanelUniform.modelViewMatrix.index )
            
            self.bufferProvider.update( value: self.projectionMatrix,
                                        at: PanelUniform.projectionMatrix.index )
            
            let uniformBuffer = self.bufferProvider.nextBuffer( )
            
            enc.setVertexBuffer( uniformBuffer, offset: 0, index: 1 )
        }
        
        enc.drawPrimitives( type: .triangle,
                             vertexStart: 0,
                             vertexCount: 6,
                             instanceCount: 2 )
    }
    
    func onDrawCompleted( )
    {
        self.bufferProvider.availableResourcesSemaphore.signal( )
    }
}

// MARK: - Node component methods
extension Panel
{
    func updateWithDelta( delta: CFTimeInterval )
    {
        self.time += delta
    }
}

// MARK: - Physical component methods
extension Panel
{
    func updateOnResize( with size: CGSize )
    {
        self.scaleX = Float( size.width / size.height )
    }
    
    func render( encoder: MTLRenderCommandEncoder )
    {
//        self.draw( encoder )
    }
}
