//
//  PanelBuilder.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

extension Panel
{
    internal static func vertices( ) -> [ VertexPosition2DTex ]
    {
        let topLeft = VertexPosition2DTex( x: -1, y: 1, s: 0, t: 0 )
        let topRight = VertexPosition2DTex( x: 1, y: 1, s: 1, t: 0 )
        let baseLeft = VertexPosition2DTex( x: -1, y: -1, s: 0, t: 1 )
        let baseRight = VertexPosition2DTex( x: 1, y: -1, s: 1, t: 1 )
        
        return [
            baseLeft, topRight, topLeft,
            baseLeft, baseRight, topRight,
        ]
    }
}
