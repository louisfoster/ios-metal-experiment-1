//
//  SceneShaderPrograms.swift
//  frameBuf
//
//  Created by Louis Foster on 4/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

struct SceneLineFilter
{
    var name = "CILineOverlay"
    
    var params = [
        "inputThreshold": 0.1,
        "inputContrast": 50.0,
        "inputNRNoiseLevel": 0.01,
        "inputNRSharpness": 1.0,
        "inputEdgeIntensity": 5.0,
    ]
}

enum SceneShaderProgramItems
{
    case cube
    case panel
    case post
}

enum SceneShaderFunctionName: String
{
    // Vertex names
    case sceneVert = "scene_vertex"
    case scenePanelVert = "scenepanel_vertex"
    case postVert = "post_vertex"
    
    // Fragment names
    case sceneFrag = "scene_fragment"
    case passthruPostFrag = "passthru_post_fragment"
    case sobelPostFrag = "sobel_post_fragment"
    case bwSobelPostFrag = "bwsobel_post_fragment"
    case bwSobelMotionPostFrag = "bwsobelmotion_post_fragment"
    case boxBlurPostFrag = "boxBlur_post_fragment"
    case grayscalePostFrag = "grayscale_post_fragment"
    
    var string: String
    {
        return self.rawValue
    }
}

struct SceneShaderPrograms
{
    var cube: MTLRenderPipelineState
    var panel: MTLRenderPipelineState
    var post: MTLRenderPipelineState
    
    static func functionNames( item: SceneShaderProgramItems )
    -> ( vert: String, frag: String? )
    {
        switch item {
        case .cube:
            return ( SceneShaderFunctionName.sceneVert.string,
                     SceneShaderFunctionName.sceneFrag.string )
        case .panel:
            return ( SceneShaderFunctionName.scenePanelVert.string,
                     SceneShaderFunctionName.passthruPostFrag.string )
        case .post:
            return ( SceneShaderFunctionName.postVert.string,
                     SceneShaderFunctionName.bwSobelMotionPostFrag.string )
        }
    }
}

// MARK: - Shader Handlers
extension Scene
{
    class internal func getShaderPrograms( _ view: MTKView )
    throws -> SceneShaderPrograms
    {
        guard let device = view.device
            else
        {
            throw SceneInitError.noDevice
        }
        
        guard let library = device.makeDefaultLibrary( )
            else
        {
            throw ShaderError.noMetalLibrary
        }
        
        let descriptor = MTLRenderPipelineDescriptor( )
        
        let cube = SceneShaderPrograms.functionNames( item: .cube )
        let sceneProgram =
            try ShaderProgramProvider
                .createShaderProgram( device: device,
                                      library: library,
                                      descriptor: descriptor,
                                      vertex: cube.vert,
                                      fragment: cube.frag,
                                      colorPixelFormat: .bgra8Unorm,
                                      depthPixelFormat: .depth32Float_stencil8,
                                      stencilPixelFormat: .depth32Float_stencil8 )
        
        let panel = SceneShaderPrograms.functionNames( item: .panel )
        let panelProgram =
            try ShaderProgramProvider
                .createShaderProgram( device: device,
                                      library: library,
                                      descriptor: descriptor,
                                      vertex: panel.vert,
                                      fragment: panel.frag,
                                      colorPixelFormat: .bgra8Unorm )
        
        let post = SceneShaderPrograms.functionNames( item: .post )
        let postProgram =
            try ShaderProgramProvider
                .createShaderProgram( device: device,
                                      library: library,
                                      descriptor: descriptor,
                                      vertex: post.vert,
                                      fragment: post.frag,
                                      colorPixelFormat: .bgra8Unorm )
        
        return SceneShaderPrograms( cube: sceneProgram,
                                    panel: panelProgram,
                                    post: postProgram )
    }
}
