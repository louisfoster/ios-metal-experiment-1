//
//  SceneCameras.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

extension Scene
{
    class internal func getCameraViews( _ view: MTKView ) throws -> CameraViews
    {
        guard let _device = view.device
            else
        {
            throw SceneInitError.noDevice
        }
        
        let dimensions = view.dimensions2D( )
        
        let _scene = Camera( name: "Scene Camera",
                             device: _device,
                             dimensions: dimensions,
                             x: 0, y: 4.0, z: 8.0 )
        
        _scene.tilt( degrees: -25.0 )
        
        let _panel = Camera( name: "Panel Camera",
                             device: _device,
                             dimensions: dimensions,
                             x: 0, y: 2.0, z: 4.0 )
        
        _panel.tilt( degrees: -30.0 )
        
        return CameraViews( scene: _scene, panel: _panel )
    }
}
