//
//  Scene.swift
//  frameBuf
//
//  Created by Louis Foster on 6/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit
import MetalPerformanceShaders
import CoreImage

struct Framebuffer
{
    var descriptor: MTLRenderPassDescriptor
    var texture: MTLTexture
}

protocol SceneProtocol
{
    var view: MTKView { get }
    var shaderPrograms: SceneShaderPrograms { get }
    var cameraViews: CameraViews { get }
}

class Scene: SceneProtocol
{
    // MARK: Properties
    
    private(set) var view: MTKView
    
    private(set) var shaderPrograms: SceneShaderPrograms
    
    private(set) var cameraViews: CameraViews
    
    // Framebuffer
    
    var framebuffer: Framebuffer
    
    var postFramebuffer: Framebuffer
    
    // Objects
    
    var panel: Panel
    
    var cube: Cube
    
    // MARK: Initialisers
    
    init( _ textureLoader: MTKTextureLoader,
          _ _view: MTKView ) throws
    {
        self.view = _view
        
        self.shaderPrograms = try Scene.getShaderPrograms( self.view )
        
        self.cameraViews = try Scene.getCameraViews( self.view )
        
        self.framebuffer = try Scene.getFramebuffer( self.view )
        
        self.postFramebuffer = try Scene.getFramebuffer( self.view )
        
        self.panel = try Panel( name: "Panel",
                                self.view,
                                [ self.framebuffer.texture,
                                  self.postFramebuffer.texture ] )
        
        let catTexture = try Texture.load( named: "cat", loader: textureLoader )
        let dogTexture = try Texture.load( named: "dog", loader: textureLoader )
        
        self.cube = try Cube( name: "Cube", self.view, [ catTexture, dogTexture ] )
    }
    
    func updateWithDelta( delta: CFTimeInterval )
    {
        self.cube.updateWithDelta( delta: delta )
    }
    
    func updateOnResize( with size: CGSize ) throws
    {
        self.framebuffer = try Scene.getFramebuffer( self.view )
        self.postFramebuffer = try Scene.getFramebuffer( self.view )
        self.panel.textures = [ self.framebuffer.texture, self.postFramebuffer.texture ]
        
        try self.cube.updateOnResize( with: size )
        self.panel.updateOnResize( with: size )
        
        self.cameraViews.updateProjectionMatrices( self.view.dimensions2D( ) )
    }
    
    func render( _ _view: MTKView,
                 _ commandQ: MTLCommandQueue )
    {
        self.view = _view
        
        // Create command buffer for render calls
        guard let commandBuffer = commandQ.makeCommandBuffer( )
            else
        {
            return
        }
        
        commandBuffer.addCompletedHandler { _ in
            self.cube.onDrawCompleted( )
            self.panel.onDrawCompleted( )
        }

        // For cube
        // This needs to be set in case they are redrawn (eg during resize)
        self.framebuffer.descriptor.depthAttachment =
            self.cube.depthStencilAttachment.depth
        
        self.framebuffer.descriptor.stencilAttachment =
            self.cube.depthStencilAttachment.stencil
        
        if let renderEncoder = commandBuffer.makeRenderCommandEncoder(
                                descriptor: self.framebuffer.descriptor )
        {
            let program = self.shaderPrograms.cube
            renderEncoder.setRenderPipelineState( program )
            
            // This needs to be set in case they are redrawn (eg during resize)
            self.cube.viewMatrix = self.cameraViews.panel.getViewMatrix( )
            self.cube.projectionMatrix = self.cameraViews.panel.projectionMatrix
            
            self.cube.render( encoder: renderEncoder )
            
            renderEncoder.endEncoding( )
            self.cube.onDrawCompleted( )
        }
        
        // Post
        
        if let renderEncoder =
            commandBuffer.makeRenderCommandEncoder( descriptor: self.postFramebuffer.descriptor )
        {
            let program = self.shaderPrograms.post
            renderEncoder.setRenderPipelineState( program )
            
            self.panel.draw( renderEncoder, post: true )
            
            renderEncoder.endEncoding( )
        }
        
        self.view.clearColor = MTLClearColor( red: 0.1,
                                              green: 0.6,
                                              blue: 0.8,
                                              alpha: 1.0 )
        
        // Drawable
 
        guard let panelDescriptor = self.view.currentRenderPassDescriptor
            else
        {
            return
        }
        
        panelDescriptor.colorAttachments[0].loadAction = .clear
        
        if let renderEncoder =
            commandBuffer.makeRenderCommandEncoder( descriptor: panelDescriptor )
        {
            let program = self.shaderPrograms.panel
            renderEncoder.setRenderPipelineState( program )
            
            self.panel.viewMatrix = self.cameraViews.scene.getViewMatrix( )
            self.panel.projectionMatrix = self.cameraViews.scene.projectionMatrix
            
            self.panel.draw( renderEncoder, post: false )
            
            renderEncoder.endEncoding( )
        }
        
        panelDescriptor.colorAttachments[0].loadAction = .load
        
        panelDescriptor.depthAttachment =
            self.cube.depthStencilAttachment.depth
        
        panelDescriptor.stencilAttachment =
            self.cube.depthStencilAttachment.stencil
        
        if let renderEncoder = commandBuffer.makeRenderCommandEncoder(
            descriptor: panelDescriptor )
        {
            let program = self.shaderPrograms.cube
            renderEncoder.setRenderPipelineState( program )
            
            // This needs to be set in case they are redrawn (eg during resize)
            self.cube.viewMatrix = self.cameraViews.scene.getViewMatrix( )
            self.cube.projectionMatrix = self.cameraViews.scene.projectionMatrix
            
            self.cube.render( encoder: renderEncoder )
            
            renderEncoder.endEncoding( )
        }
        
        // Complete drawing
        if let _drawable = self.view.currentDrawable
        {
            commandBuffer.present( _drawable )
        }
        
        commandBuffer.commit( )
    }
}

extension Scene
{
    class internal func getFramebuffer( _ view: MTKView )
    throws -> Framebuffer
    {
        guard let device = view.device
        else
        {
            throw SceneInitError.noDevice
        }
        
        let dimensions = view.dimensions2D( )
        
        // This texture is passed to the descriptor but needs to be
        // accessed later when used by the post-processing panel
        // the descriptor will be used by the cube
        let textureDescriptor =
            MTLTextureDescriptor.texture2DDescriptor( pixelFormat: .bgra8Unorm,
                                                      width: Int( dimensions.width ),
                                                      height: Int( dimensions.height ),
                                                      mipmapped: false )
        
        textureDescriptor.usage = [ .renderTarget, .shaderRead, .shaderWrite ]
        
        guard let texture = device.makeTexture( descriptor: textureDescriptor )
        else
        {
            throw SceneInitError.noFramebuffer
        }
        
        let colorAttachment = MTLRenderPassColorAttachmentDescriptor( )
        colorAttachment.clearColor = MTLClearColor( red: 1.0,
                                                    green: 1.0,
                                                    blue: 1.0,
                                                    alpha: 1.0 )
        colorAttachment.texture = texture
        colorAttachment.storeAction = .store
        colorAttachment.loadAction = .clear
        
        let renderPassDescriptor = MTLRenderPassDescriptor( )
        renderPassDescriptor.colorAttachments[ 0 ] = colorAttachment
        
        return Framebuffer( descriptor: renderPassDescriptor,
                            texture: texture )
    }
}
