//
//  PanelVertexShader.metal
//  frameBuf
//
//  Created by Louis Foster on 3/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 The intention for this vert shader is to be used with post-processing
 fragment shaders such they are rendered onto a texture
 */

#include <metal_stdlib>
using namespace metal;

#include "../Common/Post_Common.h"

struct VertexIn
{
    packed_float2 position;
    packed_float2 texcoord;
};

vertex VertexOut post_vertex( const device VertexIn *vertex_array [ [ buffer( 0 ) ] ],
                              unsigned int vid [ [ vertex_id ] ] )
{
    VertexIn VertexIn = vertex_array[ vid ];
    
    VertexOut VertexOut;
    VertexOut.position = float4( VertexIn.position, 0, 1 );
    VertexOut.texcoord = VertexIn.texcoord;
    
    return VertexOut;
}
