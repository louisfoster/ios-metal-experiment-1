//
//  SceneVertexShader.metal
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../Common/Scene_Common.h"

struct VertexIn
{
    packed_float3 position;
    packed_float4 color;
    packed_float2 texcoord;
};


vertex VertexOut scene_vertex( const device VertexIn *vertex_array [ [ buffer( 0 ) ] ],
                               const device Uniforms &uniforms [ [ buffer( 1 ) ] ],
                               unsigned int vid [ [ vertex_id ] ] )
{
    float4x4 proj_Matrix = uniforms.projectionMatrix;
    float4x4 mv_Matrix = uniforms.modelMatrix;
    
    VertexIn VertexIn = vertex_array[ vid ];
    
    VertexOut VertexOut;
    VertexOut.position = proj_Matrix * mv_Matrix * float4( VertexIn.position, 1 );
    VertexOut.color = VertexIn.color;
    VertexOut.texcoord = VertexIn.texcoord;
    
    return VertexOut;
}
