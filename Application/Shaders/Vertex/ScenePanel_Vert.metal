//
//  ScenePanel_Vert.metal
//  frameBuf
//
//  Created by Louis Foster on 8/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../Common/Post_Common.h"

struct VertexIn
{
    packed_float2 position;
    packed_float2 texcoord;
};

vertex VertexOut scenepanel_vertex( const device VertexIn *vertex_array [ [ buffer( 0 ) ] ],
                                    const device PostUniforms &uniforms [ [ buffer( 1 ) ] ],
                                    unsigned int vid [ [ vertex_id ] ] )
{
    float4x4 proj_Matrix = uniforms.projectionMatrix;
    float4x4 mv_Matrix = uniforms.modelMatrix;
    
    VertexIn VertexIn = vertex_array[ vid ];
    
    VertexOut VertexOut;
    VertexOut.position = proj_Matrix * mv_Matrix * float4( VertexIn.position, 0, 1 );
    VertexOut.texcoord = VertexIn.texcoord;
    
    return VertexOut;
}
