//
//  BWSobel_Frag.metal
//  frameBuf
//
//  From: http://kodemongki.blogspot.com/2011/06/kameraku-custom-shader-effects-example.html
//

#include <metal_stdlib>
using namespace metal;

#include "../../Common/Post_Common.h"

fragment float4 bwsobel_post_fragment( VertexOut interpolated [ [ stage_in ] ],
                                       texture2d<float> tex [ [ texture( 0 ) ] ],
                                       sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    // TODO: pass values in as uniforms to be changed
    const float step_w = 0.001; // default 0.0015625
    const float step_h = 0.002; // default 0.0027778
    const float mag = 6.0; // default 2.0
    const float threshold = 0.5; // default 0.2
    
    const float2 texcoord = interpolated.texcoord;

    float3 t1 = tex.sample( sampler2D,
                            float2( texcoord.x - step_w, texcoord.y - step_h ) ).bgr;
    
    float3 t2 = tex.sample( sampler2D,
                            float2( texcoord.x, texcoord.y - step_h ) ).bgr;
    
    float3 t3 = tex.sample( sampler2D,
                            float2( texcoord.x + step_w, texcoord.y - step_h ) ).bgr;
    
    float3 t4 = tex.sample( sampler2D,
                           float2( texcoord.x - step_w, texcoord.y ) ).bgr;
    
//    float3 t5 = tex.sample( sampler2D, texcoord ).bgr;
    
    float3 t6 = tex.sample( sampler2D,
                            float2( texcoord.x + step_w, texcoord.y ) ).bgr;
    
    float3 t7 = tex.sample( sampler2D,
                            float2( texcoord.x - step_w, texcoord.y + step_h ) ).bgr;
    
    float3 t8 = tex.sample( sampler2D,
                            float2( texcoord.x, texcoord.y + step_h ) ).bgr;
    
    float3 t9 = tex.sample( sampler2D,
                            float2( texcoord.x + step_w, texcoord.y + step_h ) ).bgr;
    
    
    float3 xx = t1 + ( mag * t2 ) + t3 - t7 - ( mag * t8 ) - t9;
    float3 yy = t1 - t3 + ( mag * t4 ) - ( mag * t6 ) + t7 - t9;
    
    float3 rr = sqrt( xx * xx + yy * yy );
    float y = ( rr.r + rr.g + rr.b ) / 3.0;
    
    
    if (y > threshold)
    {
        rr = float3( 0.0, 0.0, 0.0 );
    }
    else
    {
        rr = float3( 1.0, 1.0, 1.0 );
    }
    
    return float4( rr, 1.0 );
}

