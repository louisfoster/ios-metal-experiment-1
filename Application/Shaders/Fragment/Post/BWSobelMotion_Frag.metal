//
//  BWSobelMotion_Frag.metal
//  frameBuf
//
//  Created by Louis Foster on 8/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;


#include "../../Common/Post_Common.h"

fragment float4 bwsobelmotion_post_fragment( VertexOut interpolated [ [ stage_in ] ],
                                             array<texture2d<float>, 2> tex [ [ texture( 0 ) ] ],
                                             sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    // TODO: pass values in as uniforms to be changed
    const float step_w = 0.0015625; // default 0.0015625
    const float step_h = 0.0027778; // default 0.0027778
    
    const float mag = 6.0; // default 2.0
    const float edgeThreshold = 0.7; // default 0.2
    
    const float fadeRate = 0.2; // default 0.75
    const float fadeThreshold = 0.0001; // default 0.0001
    
    const float3 lineColor = float3( 0.0, 0.0, 0.0 ); // default white (1.0)
    const float3 fillColor = float3( 1.0, 1.0, 1.0 ); // default black (0.0)
    
    const float2 texcoord = interpolated.texcoord;
    float3 previous = tex[1].sample( sampler2D, texcoord ).rgb;
    previous += ( 1.0 - previous ) * fadeRate;
    previous = min( previous, fillColor );
    
    float3 t1 = tex[0].sample( sampler2D,
                               float2( texcoord.x - step_w, texcoord.y - step_h ) ).bgr;
    
    float3 t2 = tex[0].sample( sampler2D,
                               float2( texcoord.x, texcoord.y - step_h ) ).bgr;
    
    float3 t3 = tex[0].sample( sampler2D,
                               float2( texcoord.x + step_w, texcoord.y - step_h ) ).bgr;
    
    float3 t4 = tex[0].sample( sampler2D,
                               float2( texcoord.x - step_w, texcoord.y ) ).bgr;
    
    // float3 t5 = tex.sample( sampler2D, texcoord ).bgr;
    
    float3 t6 = tex[0].sample( sampler2D,
                               float2( texcoord.x + step_w, texcoord.y ) ).bgr;
    
    float3 t7 = tex[0].sample( sampler2D,
                               float2( texcoord.x - step_w, texcoord.y + step_h ) ).bgr;
    
    float3 t8 = tex[0].sample( sampler2D,
                               float2( texcoord.x, texcoord.y + step_h ) ).bgr;
    
    float3 t9 = tex[0].sample( sampler2D,
                               float2( texcoord.x + step_w, texcoord.y + step_h ) ).bgr;
    
    
    float3 xx = t1 + ( mag * t2 ) + t3 - t7 - ( mag * t8 ) - t9;
    float3 yy = t1 - t3 + ( mag * t4 ) - ( mag * t6 ) + t7 - t9;
    
    float3 rr = sqrt( xx * xx + yy * yy );
    float y = ( rr.r + rr.g + rr.b ) / 3.0;
    
    if ( y > edgeThreshold )
    {
        rr = lineColor;
        // TODO: render the sparkle party and output to texture for reuse
        // then draw over current output with dark lines (so fades have colors)
        // I believe this will also mean that swapping tex wont be necessary
        // rr *= distance(rr, previous); // sparkle party
    }
    else if ( distance( previous, fillColor ) > fadeThreshold )
    {
        rr = previous;
    }
    else
    {
        rr = fillColor;
    }

    return float4( rr, 1.0 );
}
