//
//  Sobel_Frag.metal
//  frameBuf
//
//  Created by Louis Foster on 3/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../../Common/Post_Common.h"


fragment float4 sobel_post_fragment( VertexOut interpolated [ [ stage_in ] ],
                                     texture2d<float> tex [ [ texture( 0 ) ] ],
                                     sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    const float sizeH = 1.0 / 300.0;
    const float sizeV = 1.0 / 200.0;
    const float2 texcoord = interpolated.texcoord;
    
    float4 top = tex.sample( sampler2D,
                             float2( texcoord.x, texcoord.y + sizeV ) );
    
    float4 base = tex.sample( sampler2D,
                              float2( texcoord.x, texcoord.y - sizeV ) );
    
    float4 left = tex.sample( sampler2D,
                              float2( texcoord.x - sizeH, texcoord.y ) );
    
    float4 right = tex.sample( sampler2D,
                               float2( texcoord.x + sizeH, texcoord.y ) );
    
    float4 topLeft = tex.sample( sampler2D,
                                 float2( texcoord.x - sizeH, texcoord.y + sizeV ) );
    
    float4 topRight = tex.sample( sampler2D,
                                  float2( texcoord.x + sizeH, texcoord.y + sizeV ) );
    
    float4 baseLeft = tex.sample( sampler2D,
                                  float2( texcoord.x - sizeH, texcoord.y - sizeV ) );
    
    float4 baseRight = tex.sample( sampler2D,
                                   float2( texcoord.x + sizeH, texcoord.y - sizeV ) );
    
    float4 sX = -topLeft - 2 * left - baseLeft + topRight + 2 * right + baseRight;
    float4 sY = -topLeft - 2 * top - topRight + baseLeft + 2 * base + baseRight;
    
    float4 sobel = sqrt( sX * sX + sY * sY );
    
    return sobel;
}
