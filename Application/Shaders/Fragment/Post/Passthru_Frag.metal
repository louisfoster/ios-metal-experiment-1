//
//  Passthru_Frag.metal
//  frameBuf
//
//  Created by Louis Foster on 3/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../../Common/Post_Common.h"

fragment float4 passthru_post_fragment( VertexOut interpolated [ [ stage_in ] ],
                                        texture2d<float> tex [ [ texture( 0 ) ] ],
                                        sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    float4 color = tex.sample( sampler2D, interpolated.texcoord );
    return color;
}
