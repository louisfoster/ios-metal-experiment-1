//
//  BoxBlur_Frag.metal
//  frameBuf
//
//  Created by Louis Foster on 3/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../../Common/Post_Common.h"

fragment float4 boxBlur_post_fragment( VertexOut interpolated [ [ stage_in ] ],
                                       texture2d<float> tex [ [ texture( 0 ) ] ],
                                       sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    const float sizeH = 1.0 / 300.0;
    const float sizeV = 1.0 / 200.0;
    const float2 texcoord = interpolated.texcoord;
    
    float4 sum = float4( 0.0 );
    
    for ( int x = -4; x <= 4; x++ )
    {
        for ( int y = -4; y <= 4; y++ )
        {
            sum += tex.sample( sampler2D,
                               float2( texcoord.x + x * sizeH,
                                       texcoord.y + y * sizeV ) ) / 81.0;
        }
    }
    
    return sum;
}
