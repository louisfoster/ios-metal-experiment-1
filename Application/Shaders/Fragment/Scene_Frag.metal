//
//  SceneFragmentShader.metal
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../Common/Scene_Common.h"

fragment float4 scene_fragment( VertexOut interpolated [ [ stage_in ] ],
                                const device Uniforms &uniforms [ [ buffer( 1 ) ] ],
                                array<texture2d<float>, 2> tex [ [ texture( 0 ) ] ],
                                sampler sampler2D [ [ sampler( 0 ) ] ] )
{
    float2 texcoord = interpolated.texcoord;
    texcoord.y = uniforms.mirrorTexture ? 1.0 - texcoord.y : texcoord.y;
    float4 tex1 = tex[0].sample( sampler2D, texcoord );
    float4 tex2 = tex[1].sample( sampler2D, texcoord );
    float4 color = mix( tex1, tex2, ( sin( uniforms.time ) + 1.0 ) * 0.5 );
    return color * interpolated.color * float4( float3( uniforms.overrideColor ), 1.0 );
}
