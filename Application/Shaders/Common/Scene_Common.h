//
//  SceneCommon.h
//  frameBuf
//
//  Created by Louis Foster on 1/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#ifndef SceneCommon_h
#define SceneCommon_h

struct VertexOut
{
    float4 position [ [ position ] ];
    float4 color;
    float2 texcoord;
};

struct Uniforms
{
    float4x4 modelMatrix;
    float4x4 projectionMatrix;
    float time;
    float overrideColor;
    bool mirrorTexture;
};

#endif /* SceneCommon_h */
