//
//  PostCommon.h
//  frameBuf
//
//  Created by Louis Foster on 3/9/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#ifndef PostCommon_h
#define PostCommon_h

struct VertexOut
{
    float4 position [ [ position ] ];
    float2 texcoord;
};

struct PostUniforms
{
    float4x4 modelMatrix;
    float4x4 projectionMatrix;
};

#endif /* PostCommon_h */
